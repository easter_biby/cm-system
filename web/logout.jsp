<%-- 
    Document   : logout
    Created on : Oct 7, 2014, 12:56:32 PM
    Author     : user
--%>



<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Logout</title>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#myModal").modal('show');
	});
</script>

</head>

<body>

<%request.getSession().setAttribute("user", null); 
session.invalidate();%>

        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                <!-- dialog body -->
                    <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <p class="text-success">You have been successfully logout.</p>
                    </div>
                </div>
            </div>
        </div>


<%
getServletContext().getRequestDispatcher("/index.jsp").include(request, response);
%>



</body>
</html>

