<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Course Management System</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
  </head>
  

<body>   
      


<%--Navigation Bar--%>
<div class="navbar navbar-inverse">
    <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="#">UTM</a>
    <a class="navbar-brand" href="#">Course Management System</a>
    </div>      
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#">ABOUT</a></li>
      <li><a href="#">LOGIN</a></li>
    </ul>
</div>
<%--End Navigation Bar--%>
  
<%--Container in index--%>      
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-8">
        <img src="images/Capture.JPG" alt="Responsive image" class="img-center center-block img-thumbnail">
    </div>
    <!--//login container -->
     <div class="col-xs-6 col-md-4">
        <div class="container-fluid">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form class="form-signin" role="form" method="post" action="login.jsp">
                    <p class="lead"><bold>Please sign in</bold></p><br>
                    <input type="text" name="name" class="form-control" placeholder="User Name" required autofocus>
                    <input type="password" name="password" class="form-control" placeholder="Password" required>
                    <select name="usertype"  class="form-control" placeholder="User Type" required>
                    <option value="select">Select User Type</option>

                    <option value=admin>admin</option>
                    <option value=lecturer>lecturer</option>
                    <option value=student>student</option>

                    </select>
                    <label class="checkbox">
                    <input type="checkbox" value="remember-me"> Remember me
                    </label>
                    <button class="btn btn-lg btn-primary btn-block" type="submit" value="submit">Sign in</button>
                    </form>
                </div>
            </div>
        </div>
     </div>
</div>  <!-- /en container -->





    
 
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <br><br><br><br><br><br><p>Copyright &copy; Application Development Team 2014</p>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html> 

