<%-- 
    Document   : insertForm
    Created on : Oct 27, 2014, 10:28:53 PM
    Author     : user
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*" %>
<%@ page autoFlush="true" buffer="1094kb"%>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Insert Course Form</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/half-slider.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script type="text/javascript">
	$(document).ready(function(){
		$("#myModal").modal('show');
	});
    </script>
  </head>
    
  <body>
      
    <%String u = (String) request.getSession().getAttribute("name");
    if (u != null ) 
    {%>
    
<%
Connection con= null;
PreparedStatement ps = null;
ResultSet rs = null;

String driverName = "com.mysql.jdbc.Driver";
String url = "jdbc:mysql://localhost:3306/coursemanagement";
String user = "root";
String password = "";

String sql = "select coursecode from course ";

try {
Class.forName(driverName);
con = DriverManager.getConnection(url, user, password);
ps = con.prepareStatement(sql);
rs = ps.executeQuery(); 
%>
    
    <%--Navigation Bar--%>
    <div class="navbar navbar-inverse">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Course Management System</a>
    </div>
    
    <div class="navbar-collapse collapse navbar-inverse-collapse">
        <ul class="nav navbar-nav">
        <li class="active"><a href="homeStudent.jsp">Home</a></li>
        <li><a href="courseStudent.jsp">Course</a></li>
        <li><a href="profileStudent.jsp">Profile</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Welcome, <%=session.getAttribute("name")%></a></li>
        <li><a href="logout.jsp">Logout</a></li>
        </ul>
    </div>
    </div>
    <%--End Navigation Bar--%>
    
    <!-- Half Page Image Background Carousel Header. Image Slider-->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
            <div class="item active">
                <!-- Set the first background image using inline CSS below. -->
                <div class="fill" style="background-image:url('Capture.JPG');"></div>
                <div class="carousel-caption">
                    <h2>Course Management System</h2>
                </div>
            </div>
            <div class="item">
                <!-- Set the second background image using inline CSS below. -->
                <div class="fill" style="background-image:url('ad.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Application Development</h2>
                </div>
            </div>
            <div class="item">
                <!-- Set the third background image using inline CSS below. -->
                <div class="fill" style="background-image:url('UTM.jpg');"></div>
                <div class="carousel-caption">
                    <h2>UTM</h2>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>

    </header>
    <%--end image slider--%>
    
    <br><br><br>
      
    <%--container--%>
    <div class="row">
        <div class="col-md-4">
            <div class="container-fluid">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">About CMS</h3>
                    </div>
                    <div class="panel-body">
                        <h3>Course Management System</h3>
                        <p>This system help user to manage the course enrollment in UTM.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="container-fluid">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Course</h3>
                    </div>
                    <div class="panel-body">
                        
                        <%--no bootstrap yet--%>
                        
                        
                        <form name="form1" method="post" action="insertmanage.jsp">
          <table width="291" border="5" cellspacing="5" cellpadding="5">
            <tr>
              <th width="67" scope="row"><div align="left">Matric Number :</div></th>
              <td width="175"><label>
                <input type="text" name="studentID" id="studentID">
              </label></td>
            </tr>
            <tr>
              <th width="67" scope="row"><div align="left">Identity Card Number</div></th>
              <td width="175"><label>
                <input type="text" name="ic" id="ic">
              </label></td>
            </tr>
            <tr>
              <th scope="row"><div align="left">Name</div></th>
              <td><label>
                <input type="text" name="name" id="name">
              </label></td>
            </tr>
            <tr>
              <th scope="row"><div align="left">Year/Programme</div></th>
              <td><select name="yearprog">
                      <option value="select">select year and programme</option>
                      <option value="1 SCSJ">1 SCSJ</option>
                      <option value="1 SCSB">1 SCSB</option>
                      <option value="1 SCSR">1 SCSR</option>
                      <option value="1 SCSI">1 SCSI</option>
                      <option value="1 SCSD">1 SCSD</option>
                      <option value="2 SCSJ">2 SCSJ</option>
                      <option value="2 SCSB">2 SCSB</option>
                      <option value="2 SCSR">2 SCSR</option>
                      <option value="2 SCSI">2 SCSI</option>
                      <option value="2 SCSD">2 SCSD</option>
                      <option value="3 SCSJ">3 SCSJ</option>
                      <option value="3 SCSB">3 SCSB</option>
                      <option value="3 SCSR">3 SCSR</option>
                      <option value="3 SCSI">3 SCSI</option>
                      <option value="3 SCSD">3 SCSD</option>
                      <option value="4 SCSJ">4 SCSJ</option>
                      <option value="4 SCSB">4 SCSB</option>
                      <option value="4 SCSR">4 SCSR</option>
                      <option value="4 SCSI">4 SCSI</option>
                      <option value="4 SCSD">4 SCSD</option>
                  </select>
              </td>
            </tr>
            <tr>
              <th scope="row"><div align="left">Session/Semester</div></th>
              <td><select name="sensem">
                      <option value="select">select session/semester</option>
                      <option value="2010/2011">2010/2011</option>
                      <option value="2011/2012">2011/2012</option>
                      <option value="2012/2013">2012/2013</option>
                      <option value="2013/2014">2013/2014</option>
                      <option value="2014/2015">2014/2015</option>
                      <option value="2015/2016">2015/2016</option>
                  </select>
              </td>
            </tr>
            <tr>
               <th scope="row"><div align="left">Subject Code</div></th>
               <td><select name="coursecode">
               <option value="select">select subject code</option>
                <%
                    while(rs.next())
                    {
                        String coursecode = rs.getString("coursecode");
                %>
                    <option value=<%=coursecode%>><%=coursecode%></option>
                <% 
                    }// end while
                    }
                    catch(SQLException sqe)
                    {
                    out.println("home"+sqe);
                    }
                    %>
                </select>
                </td>
            </tr>
            <tr>
              <th scope="row"><div align="left">Credit</div></th>
              <td><select name="credit">
                      <option value="select">select credit</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                  </select>
              </td>
            </tr>
            <tr>
              <th scope="row"><div align="left">Current cgpa</div></th>
              <td><label>
                <input type="text" name="cgpa" id="cgpa">
              </label></td>
            </tr>
            <tr>
              <th scope="row"><div align="left"></div></th>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <th scope="row"><div align="left"></div></th>
              <td><label>
                <input type="submit" name="button" id="button" value="Submit">
                <input type="reset" name="button2" id="button2" value="Reset">
              </label></td>
            </tr>
          </table>
    </form>
    <%--no bootstrap yet--%>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--end container--%>
    
    
    
    
    <%--End of Page--%>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <br><br><br><br><br><br><p>Copyright &copy; Application Development Team 2014</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <%--else if session expired--%>    
    <%}
    else
    {%>
    
        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                <!-- dialog body -->
                    <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <p class="text-info">Your session has expired. Please login again.</p>
                    </div>
                </div>
            </div>
        </div>
    
    <% getServletContext().getRequestDispatcher("/index.jsp").include(request, response);
    }%> 
    

 
 </body>
</html>


