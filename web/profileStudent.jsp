<%-- 
    Document   : profileStudent
    Created on : Oct 27, 2014, 5:37:43 PM
    Author     : user
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*" %>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Profile</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/half-slider.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script type="text/javascript">
	$(document).ready(function(){
		$("#myModal").modal('show');
	});
    </script>
  </head>
  
  <%
Connection con= null;
PreparedStatement ps = null;
ResultSet rs = null;

String driverName = "com.mysql.jdbc.Driver";
String url = "jdbc:mysql://localhost:3306/coursemanagement";
String user = "root";
String password = "";

Integer id = (Integer)request.getSession().getAttribute("id");
String sql = "SELECT * FROM student WHERE pk_id = "+id+"";

try {
Class.forName(driverName);
con = DriverManager.getConnection(url, user, password);
ps = con.prepareStatement(sql);
rs = ps.executeQuery(); 
%>

  
  <body>
      
    <%String u = (String) request.getSession().getAttribute("name");
    if (u != null ) 
    {
    rs.next();
    %>
    
    <%--Navigation Bar--%>
    <div class="navbar navbar-inverse">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Course Management System</a>
    </div>
    
    <div class="navbar-collapse collapse navbar-inverse-collapse">
        <ul class="nav navbar-nav">
        <li class="active"><a href="homeStudent.jsp">Home</a></li>
        <li><a href="courseStudent.jsp">Course</a></li>
        <li><a href="profileStudent.jsp">Profile</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Welcome, <%=session.getAttribute("name")%></a></li>
        <li><a href="logout.jsp">Logout</a></li>
        </ul>
    </div>
    </div>
    <%--End Navigation Bar--%>
    
    <!-- Half Page Image Background Carousel Header. Image Slider-->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
            <div class="item active">
                <!-- Set the first background image using inline CSS below. -->
                <div class="fill" style="background-image:url('Capture.JPG');"></div>
                <div class="carousel-caption">
                    <h2>Course Management System</h2>
                </div>
            </div>
            <div class="item">
                <!-- Set the second background image using inline CSS below. -->
                <div class="fill" style="background-image:url('ad.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Application Development</h2>
                </div>
            </div>
            <div class="item">
                <!-- Set the third background image using inline CSS below. -->
                <div class="fill" style="background-image:url('UTM.jpg');"></div>
                <div class="carousel-caption">
                    <h2>UTM</h2>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>

    </header>
    <%--end image slider--%>
    
    <br><br><br>
    

    
    <%--container--%>
    <div class="row">
        <div class="col-md-4">
            <div class="container-fluid">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">About CMS</h3>
                    </div>
                    <div class="panel-body">
                        <h3>Course Management System</h3>
                        <p>This system help user to manage the course enrollment in UTM.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="container-fluid">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Profile</h3>
                    </div>
                    <div class="panel-body">
                        <img src="images/picx.jpg" width="100" height="120" alt="Responsive image" class="img-center center-block img-thumbnail"><br><br>
                        <table class="table table-striped table-hover ">
                            <tbody>
                                <tr class="warning">
                                
                                <td>Full Name  </td>
                                <td><%=rs.getString("fullname")%></td>
                                </tr>
                                <tr class="warning">
                                
                                <td>IC No.  </td>
                                <td><%=rs.getString("ic")%></td>
                                </tr>
                                <tr class="warning">
                                
                                <td>Age  </td>
                                <td><%=rs.getString("age")%></td>
                                </tr>
                                <tr class="warning">
                                
                                <td>Matric No.  </td>
                                <td><%=rs.getString("matric_no")%></td>
                                </tr>
                            </tbody>
                        </table> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--end container--%>
    
    
    
    
    
    
    <%--End of Page--%>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <br><br><br><br><br><br><p>Copyright &copy; Application Development Team 2014</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <%--else if session expired--%>    
    <%}
    else
    {%>
    
        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                <!-- dialog body -->
                    <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <p class="text-info">Your session has expired. Please login again.</p>
                    </div>
                </div>
            </div>
        </div>
    
    <% getServletContext().getRequestDispatcher("/index.jsp").include(request, response);
    }%> 
    
<%}
catch(SQLException sqe)
{
out.println("home"+sqe);
}
%>  
 </body>
</html>


