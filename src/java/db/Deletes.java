/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.*;
import java.sql.*;

import jdbc.*;

/**
 *
 * @author U
 */
@WebServlet(name = "Deletes", urlPatterns = {"/Deletes"})
public class Deletes extends HttpServlet {
    
    private JDBCUtility2 jdbcUtility;
    private Connection con;
    
    public void init() throws ServletException
    {
        String driver = "com.mysql.jdbc.Driver";

        String dbName = "insertform";
        String url = "jdbc:mysql://localhost/" + dbName + "?";
        String userName = "root";
        String password = "";

        jdbcUtility = new JDBCUtility2(driver,
                                      url,
                                      userName,
                                      password);

        jdbcUtility.jdbcConnect();
        con = jdbcUtility.jdbcGetConnection();
    }     
    

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        String matriks = request.getParameter("matriks");
        String ic = request.getParameter("ic");
        String name = request.getParameter("name");        
        String faculty = request.getParameter("faculty");
        String yearprog = request.getParameter("yearprog");
        String session = request.getParameter("session");
        String coursecode = request.getParameter("coursecode");        
        String section = request.getParameter("section");
        String credit = request.getParameter("credit");        
        String cgpa = request.getParameter("cgpa");
        
        //create SQL query
        String sqlInsert = "INSERT INTO delete(matriks, ic, name, faculty, yearprog, session, coursecode, section, credit, cgpa)" +
                           " VALUES('" + matriks + "', " + ic + ", '" + name + "', '" + faculty + "','" + yearprog + "', " + 
                                         session + ", '" + coursecode + "', " + section + ", " + credit + ", " + cgpa + ")";
        
        boolean deleteSuccess =  jdbcUtility.jdbcDelete(sqlInsert, con);
        
        try {
            /* TODO output your page here. You may use following sample code. */
            if (deleteSuccess)
            {
                out.println("<h1><span style=\"color:deeppink\">\n" + "<em><font size=\"30\">\n" + "Course Management System\n" + "</font></em>\n" + "</span></h1>");
                out.println("<p>Student matriks  : " + matriks + "</p>");
                out.println("<p>Student matriks  : " + matriks + "</p>");
                out.println("<p>Student ic     : " + ic + "</p>");
                out.println("<p>Student name       : " + name + "</p>");
                out.println("<p>Student faculty      : " + faculty + "</p>");
                out.println("<p>Student year/prog  : " + yearprog + "</p>");
                out.println("<p>Student session/semester     : " + session + "</p>");
                out.println("<p>Student subject code       : " + coursecode + "</p>");
                out.println("<p>Student section      : " + section + "</p>");
                out.println("<p>Student credit       : " + credit + "</p>");
                out.println("<p>Student cgpa      : " + cgpa + "</p>");
                out.println("<p><a href=\"welcome.jsp\">Home</a>");
            }
            else
            {
                out.println("<p>Data insertion failed - maybe PK violation!</p>" +
                            "<p>SQL Command: " + sqlInsert + "</p>");
            }           
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
