package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE html>\r\n");
      out.write("<!--\r\n");
      out.write("To change this license header, choose License Headers in Project Properties.\r\n");
      out.write("To change this template file, choose Tools | Templates\r\n");
      out.write("and open the template in the editor.\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html lang=\"en\">\r\n");
      out.write("  <head>\r\n");
      out.write("    <meta charset=\"utf-8\">\r\n");
      out.write("    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n");
      out.write("    <meta name=\"description\" content=\"\">\r\n");
      out.write("    <meta name=\"author\" content=\"\">\r\n");
      out.write("    <link rel=\"icon\" href=\"../../favicon.ico\">\r\n");
      out.write("\r\n");
      out.write("    <title>Course Management System</title>\r\n");
      out.write("\r\n");
      out.write("    <!-- Bootstrap -->\r\n");
      out.write("    <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">\r\n");
      out.write("    \r\n");
      out.write("    <!-- Custom styles for this template -->\r\n");
      out.write("    <link href=\"signin.css\" rel=\"stylesheet\">\r\n");
      out.write("  </head>\r\n");
      out.write("  \r\n");
      out.write("\r\n");
      out.write("<body>   \r\n");
      out.write("      \r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<div class=\"navbar navbar-inverse\">\r\n");
      out.write("    <div class=\"navbar-header\">\r\n");
      out.write("    <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-inverse-collapse\">\r\n");
      out.write("      <span class=\"icon-bar\"></span>\r\n");
      out.write("      <span class=\"icon-bar\"></span>\r\n");
      out.write("      <span class=\"icon-bar\"></span>\r\n");
      out.write("    </button>\r\n");
      out.write("    <a class=\"navbar-brand\" href=\"#\">UTM</a>\r\n");
      out.write("    <a class=\"navbar-brand\" href=\"#\">Course Management System</a>\r\n");
      out.write("    </div>      \r\n");
      out.write("    <ul class=\"nav navbar-nav navbar-right\">\r\n");
      out.write("      <li><a href=\"#\">ABOUT</a></li>\r\n");
      out.write("      <li><a href=\"#\">LOGIN</a></li>\r\n");
      out.write("    </ul>\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("  \r\n");
      out.write("      \r\n");
      out.write("<div class=\"row\">\r\n");
      out.write("    <div class=\"col-xs-12 col-sm-6 col-md-8\">\r\n");
      out.write("        <img src=\"images/Capture.JPG\" alt=\"Responsive image\" class=\"img-center center-block img-thumbnail\">\r\n");
      out.write("    </div>\r\n");
      out.write("    <!--//login container -->\r\n");
      out.write("     <div class=\"col-xs-6 col-md-4\">\r\n");
      out.write("        <div class=\"container-fluid\">\r\n");
      out.write("            <div class=\"panel panel-primary\">\r\n");
      out.write("                <div class=\"panel-body\">\r\n");
      out.write("                    <form class=\"form-signin\" role=\"form\" method=\"post\" action=\"login.jsp\">\r\n");
      out.write("                    <p class=\"lead\"><bold>Please sign in</bold></p><br>\r\n");
      out.write("                    <input type=\"text\" name=\"name\" class=\"form-control\" placeholder=\"User Name\" required autofocus>\r\n");
      out.write("                    <input type=\"password\" name=\"password\" class=\"form-control\" placeholder=\"Password\" required>\r\n");
      out.write("                    <select name=\"usertype\"  class=\"form-control\" placeholder=\"User Type\" required>\r\n");
      out.write("                    <option value=\"select\">Select User Type</option>\r\n");
      out.write("\r\n");
      out.write("                    <option value=admin>admin</option>\r\n");
      out.write("                    <option value=lecturer>lecturer</option>\r\n");
      out.write("                    <option value=student>student</option>\r\n");
      out.write("\r\n");
      out.write("                    </select>\r\n");
      out.write("                    <label class=\"checkbox\">\r\n");
      out.write("                    <input type=\"checkbox\" value=\"remember-me\"> Remember me\r\n");
      out.write("                    </label>\r\n");
      out.write("                    <button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\" value=\"submit\">Sign in</button>\r\n");
      out.write("                    </form>\r\n");
      out.write("                </div>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("     </div>\r\n");
      out.write("</div>  <!-- /en container -->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("    \r\n");
      out.write(" \r\n");
      out.write("<footer>\r\n");
      out.write("    <div class=\"container\">\r\n");
      out.write("        <div class=\"row\">\r\n");
      out.write("            <div class=\"col-lg-12 text-center\">\r\n");
      out.write("                <br><br><br><br><br><br><p>Copyright &copy; Application Development Team 2014</p>\r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("    </div>\r\n");
      out.write("</footer>\r\n");
      out.write("\r\n");
      out.write("<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->\r\n");
      out.write("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>\r\n");
      out.write("<!-- Include all compiled plugins (below), or include individual files as needed -->\r\n");
      out.write("<script src=\"js/bootstrap.min.js\"></script>\r\n");
      out.write("</body>\r\n");
      out.write("</html> \r\n");
      out.write("\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
