package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;

public final class insertForm_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

String driverName = "com.mysql.jdbc.Driver";
String url = "jdbc:mysql://localhost:3306/coursemanagement";
String user = "root";
String psw = "";
  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 1120256, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<!--\n");
      out.write("To change this license header, choose License Headers in Project Properties.\n");
      out.write("To change this template file, choose Tools | Templates\n");
      out.write("and open the template in the editor.\n");
      out.write("-->\n");
      out.write("<html lang=\"en\">\n");
      out.write("  <head>\n");
      out.write("    <meta charset=\"utf-8\">\n");
      out.write("    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n");
      out.write("    <title>Insert Course Form</title>\n");
      out.write("    <!-- Bootstrap -->\n");
      out.write("    <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">\n");
      out.write("    <!-- Custom CSS -->\n");
      out.write("    <link href=\"css/half-slider.css\" rel=\"stylesheet\">\n");
      out.write("    <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>\n");
      out.write("    <script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js\"></script>\n");
      out.write("    <script type=\"text/javascript\">\n");
      out.write("\t$(document).ready(function(){\n");
      out.write("\t\t$(\"#myModal\").modal('show');\n");
      out.write("\t});\n");
      out.write("    </script>\n");
      out.write("  </head>\n");
      out.write("    \n");
      out.write("  <body>\n");
      out.write("      \n");
      out.write("    ");
String u = (String) request.getSession().getAttribute("name");
    if (u != null ) 
    {
      out.write("\n");
      out.write("    \n");
      out.write("    ");
      out.write("\n");
      out.write("    <div class=\"navbar navbar-inverse\">\n");
      out.write("    <div class=\"navbar-header\">\n");
      out.write("        <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-inverse-collapse\">\n");
      out.write("            <span class=\"icon-bar\"></span>\n");
      out.write("            <span class=\"icon-bar\"></span>\n");
      out.write("            <span class=\"icon-bar\"></span>\n");
      out.write("        </button>\n");
      out.write("        <a class=\"navbar-brand\" href=\"#\">Course Management System</a>\n");
      out.write("    </div>\n");
      out.write("    \n");
      out.write("    <div class=\"navbar-collapse collapse navbar-inverse-collapse\">\n");
      out.write("        <ul class=\"nav navbar-nav\">\n");
      out.write("        <li class=\"active\"><a href=\"homeStudent.jsp\">Home</a></li>\n");
      out.write("        <li><a href=\"courseStudent.jsp\">Course</a></li>\n");
      out.write("        <li><a href=\"profileStudent.jsp\">Profile</a></li>\n");
      out.write("        </ul>\n");
      out.write("        <ul class=\"nav navbar-nav navbar-right\">\n");
      out.write("        <li><a href=\"#\">Welcome, ");
      out.print(session.getAttribute("name"));
      out.write("</a></li>\n");
      out.write("        <li><a href=\"logout.jsp\">Logout</a></li>\n");
      out.write("        </ul>\n");
      out.write("    </div>\n");
      out.write("    </div>\n");
      out.write("    ");
      out.write("\n");
      out.write("    \n");
      out.write("    <!-- Half Page Image Background Carousel Header. Image Slider-->\n");
      out.write("    <header id=\"myCarousel\" class=\"carousel slide\">\n");
      out.write("        <!-- Indicators -->\n");
      out.write("        <ol class=\"carousel-indicators\">\n");
      out.write("            <li data-target=\"#myCarousel\" data-slide-to=\"0\" class=\"active\"></li>\n");
      out.write("            <li data-target=\"#myCarousel\" data-slide-to=\"1\"></li>\n");
      out.write("            <li data-target=\"#myCarousel\" data-slide-to=\"2\"></li>\n");
      out.write("        </ol>\n");
      out.write("\n");
      out.write("        <!-- Wrapper for Slides -->\n");
      out.write("        <div class=\"carousel-inner\">\n");
      out.write("            <div class=\"item active\">\n");
      out.write("                <!-- Set the first background image using inline CSS below. -->\n");
      out.write("                <div class=\"fill\" style=\"background-image:url('Capture.JPG');\"></div>\n");
      out.write("                <div class=\"carousel-caption\">\n");
      out.write("                    <h2>Course Management System</h2>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"item\">\n");
      out.write("                <!-- Set the second background image using inline CSS below. -->\n");
      out.write("                <div class=\"fill\" style=\"background-image:url('ad.jpg');\"></div>\n");
      out.write("                <div class=\"carousel-caption\">\n");
      out.write("                    <h2>Application Development</h2>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            <div class=\"item\">\n");
      out.write("                <!-- Set the third background image using inline CSS below. -->\n");
      out.write("                <div class=\"fill\" style=\"background-image:url('UTM.jpg');\"></div>\n");
      out.write("                <div class=\"carousel-caption\">\n");
      out.write("                    <h2>UTM</h2>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("        <!-- Controls -->\n");
      out.write("        <a class=\"left carousel-control\" href=\"#myCarousel\" data-slide=\"prev\">\n");
      out.write("            <span class=\"icon-prev\"></span>\n");
      out.write("        </a>\n");
      out.write("        <a class=\"right carousel-control\" href=\"#myCarousel\" data-slide=\"next\">\n");
      out.write("            <span class=\"icon-next\"></span>\n");
      out.write("        </a>\n");
      out.write("\n");
      out.write("    </header>\n");
      out.write("    ");
      out.write("\n");
      out.write("    \n");
      out.write("    <br><br><br>\n");
      out.write("      \n");
      out.write("    ");
      out.write("\n");
      out.write("    <div class=\"row\">\n");
      out.write("        <div class=\"col-md-4\">\n");
      out.write("            <div class=\"container-fluid\">\n");
      out.write("                <div class=\"panel panel-primary\">\n");
      out.write("                    <div class=\"panel-heading\">\n");
      out.write("                        <h3 class=\"panel-title\">About CMS</h3>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"panel-body\">\n");
      out.write("                        <h3>Course Management System</h3>\n");
      out.write("                        <p>This system help user to manage the course enrollment in UTM.</p>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"col-md-8\">\n");
      out.write("            <div class=\"container-fluid\">\n");
      out.write("                <div class=\"panel panel-primary\">\n");
      out.write("                    <div class=\"panel-heading\">\n");
      out.write("                        <h3 class=\"panel-title\">Course</h3>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"panel-body\">\n");
      out.write("                        \n");
      out.write("                        ");
      out.write("\n");
      out.write("                        \n");
      out.write("                        \n");
      out.write("                        <form name=\"form1\" method=\"post\" action=\"insertForm.jsp\">\n");
      out.write("          <table width=\"291\" border=\"5\" cellspacing=\"5\" cellpadding=\"5\">\n");
      out.write("            <tr>\n");
      out.write("              <th width=\"67\" scope=\"row\"><div align=\"left\">Matric Number :</div></th>\n");
      out.write("              <td width=\"175\"><label>\n");
      out.write("                <input type=\"text\" name=\"studentID\" id=\"studentID\">\n");
      out.write("              </label></td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("              <th width=\"67\" scope=\"row\"><div align=\"left\">Identity Card Number</div></th>\n");
      out.write("              <td width=\"175\"><label>\n");
      out.write("                <input type=\"text\" name=\"ic\" id=\"ic\">\n");
      out.write("              </label></td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("              <th scope=\"row\"><div align=\"left\">Name</div></th>\n");
      out.write("              <td><label>\n");
      out.write("                <input type=\"text\" name=\"name\" id=\"name\">\n");
      out.write("              </label></td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("              <th scope=\"row\"><div align=\"left\">Year/Programme</div></th>\n");
      out.write("              <td><label>\n");
      out.write("                <input type=\"text\" name=\"yearprog\" id=\"yearprog\">\n");
      out.write("              </label></td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("              <th scope=\"row\"><div align=\"left\">Session/Semester</div></th>\n");
      out.write("              <td><label>\n");
      out.write("                <input type=\"text\" name=\"sensem\" id=\"sensem\">\n");
      out.write("              </label></td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("              <th scope=\"row\"><div align=\"left\">Subject Code</div></th>\n");
      out.write("              <td><label>\n");
      out.write("                <input type=\"text\" name=\"coursecode\" id=\"coursecode\">\n");
      out.write("              </label></td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("              <th scope=\"row\"><div align=\"left\">Credit</div></th>\n");
      out.write("              <td><label>\n");
      out.write("                <input type=\"text\" name=\"credit\" id=\"credit\">\n");
      out.write("              </label></td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("              <th scope=\"row\"><div align=\"left\">Current cgpa</div></th>\n");
      out.write("              <td><label>\n");
      out.write("                <input type=\"text\" name=\"cgpa\" id=\"cgpa\">\n");
      out.write("              </label></td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("              <th scope=\"row\"><div align=\"left\"></div></th>\n");
      out.write("              <td>&nbsp;</td>\n");
      out.write("            </tr>\n");
      out.write("            <tr>\n");
      out.write("              <th scope=\"row\"><div align=\"left\"></div></th>\n");
      out.write("              <td><label>\n");
      out.write("                <input type=\"submit\" name=\"button\" id=\"button\" value=\"Submit\">\n");
      out.write("                <input type=\"reset\" name=\"button2\" id=\"button2\" value=\"Reset\">\n");
      out.write("              </label></td>\n");
      out.write("            </tr>\n");
      out.write("          </table>\n");
      out.write("    </form>\n");
      out.write("    ");
      out.write("\n");
      out.write("\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("    ");
      out.write("\n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    \n");
      out.write("    ");
      out.write("\n");
      out.write("    <footer>\n");
      out.write("        <div class=\"container\">\n");
      out.write("            <div class=\"row\">\n");
      out.write("                <div class=\"col-lg-12 text-center\">\n");
      out.write("                    <br><br><br><br><br><br><p>Copyright &copy; Application Development Team 2014</p>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    </footer>\n");
      out.write("\n");
      out.write("    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->\n");
      out.write("    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>\n");
      out.write("    <!-- Include all compiled plugins (below), or include individual files as needed -->\n");
      out.write("    <script src=\"js/bootstrap.min.js\"></script>\n");
      out.write("\n");
      out.write("    ");
      out.write("    \n");
      out.write("    ");
}
    else
    {
      out.write("\n");
      out.write("    \n");
      out.write("        <div id=\"myModal\" class=\"modal fade\">\n");
      out.write("            <div class=\"modal-dialog\">\n");
      out.write("                <div class=\"modal-content\">\n");
      out.write("                <!-- dialog body -->\n");
      out.write("                    <div class=\"modal-body\">\n");
      out.write("                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n");
      out.write("                    <p class=\"text-info\">Your session has expired. Please login again.</p>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    \n");
      out.write("    ");
 getServletContext().getRequestDispatcher("/index.jsp").include(request, response);
    }
      out.write(" \n");
      out.write("    \n");
      out.write('\n');
      out.write('\n');
      out.write('\n');
      out.write('\n');
 String studentID = request.getParameter("studentID");
   String ic = request.getParameter("ic");
   String name = request.getParameter("name");
   String yearprog = request.getParameter("yearprog");
   String sensem = request.getParameter("sensem");
   String coursecode = request.getParameter("coursecode");
   String credit = request.getParameter("credit");  
   String cgpa = request.getParameter("cgpa");  
   String status;
   
   String act = request.getParameter("submit"); //act change to butt name
    if(studentID!= null && ic!=null && name!= null && yearprog!= null && sensem!=null && coursecode!= null && credit!= null && cgpa!=null )
    {
        Connection con = DriverManager.getConnection(url,user,psw);
        PreparedStatement ps = null;
        int updateQuery = 0;
        try
        {
            Class.forName(driverName);
            //con = DriverManager.getConnection(url,user,psw);
			
			
				String queryString = "INSERT INTO insertform(studentID, ic, name, yearprog, sensem, coursecode, credit, cgpa, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
              
                      /* createStatement() is used for create statement object that is used for  sending sql statements to the specified database. */
				ps = con.prepareStatement(queryString);
				ps.setString(1, studentID);
				ps.setString(2, ic);
                                ps.setString(3, name);
                                ps.setString(4, yearprog);
				ps.setString(5, sensem);
                                ps.setString(6, coursecode);
                                ps.setString(7, credit);
				ps.setString(8, cgpa);
                                ps.setString(9, "processing");
                                
				//ps.setString(3, address);
				int i = ps.executeUpdate();
                            if(i > 0)
					{
						
      out.write("\n");
      out.write("\t\t\t\t\t\t");
      if (true) {
        _jspx_page_context.forward("insertself.jsp");
        return;
      }
      out.write("\n");
      out.write("\t\t\t\t\t\t");
 
					}
				else
					{
						
      out.write("\n");
      out.write("\t\t\t\t\t\t");
      if (true) {
        _jspx_page_context.forward("insertForm.jsp");
        return;
      }
      out.write("\n");
      out.write("\t\t\t\t\t\t");

					}
			
                        
                   			
        }
    catch(SQLException sqe)
    {
        request.setAttribute("error", sqe);
        out.println(sqe);
    }
}

      out.write("\n");
      out.write("\n");
      out.write(" \n");
      out.write(" </body>\n");
      out.write("</html>\n");
      out.write("\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
