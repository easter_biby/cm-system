<%-- 
    Document   : displayInsertApp
    Created on : Oct 29, 2014, 10:06:06 PM
    Author     : user
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*" %>

<%!String driverName = "com.mysql.jdbc.Driver";%>
<%!String url = "jdbc:mysql://localhost:3306/coursemanagement";%>
<%!String user = "root";%>
<%!String psw = "";%>
<% 
Connection connection = null;
Statement statement = null;
ResultSet rs = null;
PreparedStatement ps = null;

Class.forName("com.mysql.jdbc.Driver").newInstance();

connection = DriverManager.getConnection(url, user, psw);
statement = connection.createStatement();

String studid = request.getParameter("MatricNo");
String studID = (String)session.getAttribute("MatricNo");


String selectID = "Select * From insertform where studentID = '"+studid+"'";
//out.println(selectID);

   //String address = request.getParameter("address");  

        
        
        int updateQuery = 0;
        rs = statement.executeQuery(selectID);
        rs.next();
        

%>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Course</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/half-slider.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script type="text/javascript">
	$(document).ready(function(){
		$("#myModal").modal('show');
	});
    </script>
  </head>
    
  <body>
      
    <%String u = (String) request.getSession().getAttribute("name");
    if (u != null ) 
    {
    try{%>
    
    <%--Navigation Bar--%>
    <div class="navbar navbar-inverse">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Course Management System</a>
    </div>
    
    <div class="navbar-collapse collapse navbar-inverse-collapse">
        <ul class="nav navbar-nav">
        <li class="active"><a href="homeLecturer.jsp">Home</a></li>
        <li><a href="courseLecturer.jsp">Course</a></li>
        <li><a href="profileLecturer.jsp">Profile</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Welcome, <%=session.getAttribute("name")%></a></li>
        <li><a href="logout.jsp">Logout</a></li>
        </ul>
    </div>
    </div>
    <%--End Navigation Bar--%>
    
    <!-- Half Page Image Background Carousel Header. Image Slider-->
    <header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
            <div class="item active">
                <!-- Set the first background image using inline CSS below. -->
                <div class="fill" style="background-image:url('Capture.JPG');"></div>
                <div class="carousel-caption">
                    <h2>Course Management System</h2>
                </div>
            </div>
            <div class="item">
                <!-- Set the second background image using inline CSS below. -->
                <div class="fill" style="background-image:url('ad.jpg');"></div>
                <div class="carousel-caption">
                    <h2>Application Development</h2>
                </div>
            </div>
            <div class="item">
                <!-- Set the third background image using inline CSS below. -->
                <div class="fill" style="background-image:url('UTM.jpg');"></div>
                <div class="carousel-caption">
                    <h2>UTM</h2>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>

    </header>
    <%--end image slider--%>
    
    <br><br><br>
      
    <%--container--%>
    <div class="row">
        <div class="col-md-4">
            <div class="container-fluid">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">About CMS</h3>
                    </div>
                    <div class="panel-body">
                        <h3>Course Management System</h3>
                        <p>This system help user to manage the course enrollment in UTM.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="container-fluid">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Course</h3>
                    </div>
                    <div class="panel-body">
                    
                        
                        <p>Matric No : <%=rs.getString("studentID")%></p>
                        <p>IC : <%=rs.getString("ic")%></p>
                        <p>Name : <%=rs.getString("name")%></p>
                        <p>Subject Code : <%=rs.getString("coursecode")%></p>
                        <p>Current CGPA : <%=rs.getString("cgpa")%></p>
                        
                        
                        <form action="statusInsert.jsp?MatricNo=<%=rs.getString("studentID")%>" method="POST"><p>Status Form    :
                        <input type="radio" name="statusform" value="Approve" checked="checked" />Approve
                        <input type="radio" name="statusform" value="notApprove" />Not Approve</p>

                        <p>
                        <input name="Submit" type="submit" id="Submit" value="Update" >
        
                        </p>
                        </form>

                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--end container--%>
    
    
    
    
    <%--End of Page--%>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <br><br><br><br><br><br><p>Copyright &copy; Application Development Team 2014</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <%--else if session expired--%>    
    <%     rs.close();
           statement.close();
           connection.close();             
       }
    catch(SQLException sqe)
    {
        request.setAttribute("error", sqe);
        out.println(sqe);
    }
 }
    else
    {%>
    
        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                <!-- dialog body -->
                    <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <p class="text-info">Your session has expired. Please login again.</p>
                    </div>
                </div>
            </div>
        </div>
    
    <% getServletContext().getRequestDispatcher("/index.jsp").include(request, response);
    }%> 
    
        
        
  
 
 </body>
</html>



