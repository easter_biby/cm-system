<%-- 
    Document   : login
    Created on : Oct 7, 2014, 12:45:31 PM
    Author     : user
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.*" %> 
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script type="text/javascript">
	$(document).ready(function(){
		$("#myModal").modal('show');
	});
    </script>
    
</head>
<body>
<%! String userdbName;
String userdbPsw;
String dbUsertype;
int id;
String admin = "admin";
String lecturer = "lecturer";
String student = "student";
%>
<%
Connection con= null;
PreparedStatement ps = null;
ResultSet rs = null;

String driverName = "com.mysql.jdbc.Driver";
String url = "jdbc:mysql://localhost:3306/coursemanagement";
String user = "root";
String dbpsw = "";

String name = request.getParameter("name");
String password = request.getParameter("password");
String usertype = request.getParameter("usertype");

if((!(name.equals(null) || name.equals("")) && !(password.equals(null) || password.equals(""))) && !usertype.equals("select"))
{
try{
Class.forName(driverName);
con = DriverManager.getConnection(url, user, dbpsw);

if (usertype.equals("admin"))
{
    
    String sql = "select * from admin where username=? and password=?";

    ps = con.prepareStatement(sql);
    ps.setString(1, name);
    ps.setString(2, password);

    rs = ps.executeQuery();
    
    if(rs.next())
    { 
        userdbName = rs.getString("username");
        userdbPsw = rs.getString("password");
        id = rs.getInt("pk_id");

        if(name.equals(userdbName) && password.equals(userdbPsw))
        {
            session.setAttribute("name",userdbName);
            session.setAttribute("id", id);
            response.sendRedirect("home.jsp"); 
        }
    }
    else
    response.sendRedirect("error.jsp");
    rs.close();
    ps.close();

}

else if (usertype.equals("lecturer"))
{
    
    String sql = "select * from lecturer where username=? and password=?";

    ps = con.prepareStatement(sql);
    ps.setString(1, name);
    ps.setString(2, password);
    
    rs = ps.executeQuery();
    
    if(rs.next())
    { 
        userdbName = rs.getString("username");
        userdbPsw = rs.getString("password");
        id = rs.getInt("pk_id");
        
        if(name.equals(userdbName) && password.equals(userdbPsw))
        {
            session.setAttribute("name",userdbName); 
            session.setAttribute("id", id);
            response.sendRedirect("homeLecturer.jsp"); 
        }
    }
    else
    response.sendRedirect("error.jsp");
    rs.close();
    ps.close(); 

}

else if (usertype.equals("student"))
{
    
    String sql = "select * from student where username=? and password=?";

    ps = con.prepareStatement(sql);
    ps.setString(1, name);
    ps.setString(2, password);
    
    rs = ps.executeQuery();
    
    if(rs.next())
    { 
        userdbName = rs.getString("username");
        userdbPsw = rs.getString("password");
        id = rs.getInt("pk_id");
        
        if(name.equals(userdbName) && password.equals(userdbPsw))
        {
            session.setAttribute("name",userdbName);
            session.setAttribute("id", id);
            response.sendRedirect("homeStudent.jsp"); 
        } 
    }
    else
    response.sendRedirect("error.jsp");
    rs.close();
    ps.close(); 
}
}
catch(SQLException sqe)
{
out.println(sqe);
} 
}
else
{
%>

        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                <!-- dialog body -->
                    <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <p class="text-primary">Error In Login</p>
                    </div>
                </div>
            </div>
        </div>

<% 
getServletContext().getRequestDispatcher("/index.jsp").include(request, response);
}
%>
</body>
</html>

